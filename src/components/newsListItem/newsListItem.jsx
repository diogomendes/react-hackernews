import React from 'react';
import PropTypes from 'prop-types';
import Story from './story';
import Question from './question';
import './newsListItem.scss';

function NewsListItem(props) {
  const { url } = props;

  return url ?
    <Story {...props}/> :
    <Question {...props}/>;
}

NewsListItem.propTypes = {
  by: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  text: PropTypes.string,
  time: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  url: PropTypes.string
}

export default NewsListItem;
