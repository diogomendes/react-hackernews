import React from 'react';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';

function Story({ title, url, by, time }) {

  const formattedUrl = new URL(url);
  const host = formattedUrl.host;

  return (
    <li className="newslistitem">
      <div className="newslistitem-header">
        <a className="newslistitem-title" href={url} target="_blank">{title}</a>
        <span className="newslistitem-hostname">({host})</span>
      </div>
      <div className="newslistitem-details">
        By {by} &bull;&nbsp;
        <TimeAgo date={time * 1000}/>
      </div>
    </li>
  );
}

Story.propTypes = {
  by: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  time: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
}

export default Story;
