import React from 'react';
import { shallow } from 'enzyme';
import Story from '../story';
import { mockStory } from '../../../../__mocks__/mocks';

describe('story', () => {
  it('renders correctly', () => {
    const story = shallow(<Story {...mockStory}/>);

    expect(story).toMatchSnapshot();
  });
});
