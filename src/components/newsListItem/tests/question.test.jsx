import React from 'react';
import { shallow } from 'enzyme';
import Question from '../question';
import { mockQuestion } from '../../../../__mocks__/mocks';

describe('question', () => {
  it('renders correctly', () => {
    const question = shallow(<Question {...mockQuestion}/>);

    expect(question).toMatchSnapshot();
  });

  it('shows/hides the question body when clicking on the title', () => {
    const question = shallow(<Question {...mockQuestion}/>);

    // body is shown
    question.find('.newslistitem-title').simulate('click');

    expect(question).toMatchSnapshot();

    // body is hidden again
    question.find('.newslistitem-title').simulate('click');

    expect(question).toMatchSnapshot();
  });
});
