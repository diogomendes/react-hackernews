import React from 'react';
import { shallow } from 'enzyme';
import NewsListItem from '../newsListItem';
import { mockStory, mockQuestion } from '../../../../__mocks__/mocks';

describe('newsListItem', () => {
  it('renders a story', () => {
    const story = shallow(<NewsListItem {...mockStory}/>);

    expect(story).toMatchSnapshot();
  });

  it('renders a question', () => {
    const question = shallow(<NewsListItem {...mockQuestion}/>);

    expect(question).toMatchSnapshot();
  });
});
