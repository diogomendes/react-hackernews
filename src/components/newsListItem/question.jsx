import React from 'react';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';

class Question extends React.Component {
  constructor() {
    super();

    this.state = {
      showBody: false,
      isVisited: false
    };

    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.setState({
      showBody: !this.state.showBody,
      isVisited: true
    });
  }

  getBody() {
    return { __html: this.props.text };
  }

  render() {
    const { title, text, by, time } = this.props;
    const { showBody, isVisited } = this.state;

    const titleClassNames = isVisited ? 'newslistitem-title newslistitem-title--visited' : 'newslistitem-title';

    return (
      <li className="newslistitem">
        <div className={titleClassNames} onClick={this.onClick}>{title}</div>
        <div className="newslistitem-details">
          By {by} &bull;&nbsp;
          <TimeAgo date={time * 1000}/>
        </div>
        {(showBody && text) && 
          <div className="newslistitem-body" dangerouslySetInnerHTML={this.getBody()}/>
        }
      </li>
    );
  }
}

Question.propTypes = {
  by: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  text: PropTypes.string,
  time: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired
}

export default Question;
