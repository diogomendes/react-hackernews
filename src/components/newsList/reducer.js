import * as newsListActions from './actions';

/**
 * have isOnline in newsList's reducer for simplicity's sake.
 * in a larger scale app, it should be extracted as it is a separate concern
 */
const initialState = {
  isOnline: true,
  storyIds: [],
  stories: {},
  isLoading: false,
  shouldLoadMoreStories: true,
};

function newsList(state = initialState, action) {
  switch (action.type) {
    case newsListActions.LOAD_STORY_IDS_SUCCESS:
      return { ...state, storyIds: action.data };

    case newsListActions.LOAD_STORY_SUCCESS:
      const stories = { ...state.stories };
      stories[action.data.id] = action.data;

      return {
        ...state,
        stories
      };

    case newsListActions.SET_LOADING_STATUS:
      return { ...state, isLoading: action.status };

    case newsListActions.SHOULD_LOAD_MORE:
      return { ...state, shouldLoadMoreStories: action.shouldLoadMoreStories };

    case newsListActions.SET_IS_ONLINE:
    return { ...state, isOnline: action.isOnline };

    default:
      return state
  }
}

export default newsList;
