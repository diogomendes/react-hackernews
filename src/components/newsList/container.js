import { connect } from 'react-redux';
import * as actions from './actions';
import * as selectors from './selectors';
import NewsList from './newsList';

const mapStateToProps = state => {
  return {
    stories: selectors.getSortedStories(state),
    isLoading: selectors.getIsLoading(state),
    shouldLoadMoreStories: selectors.getShouldLoadMoreStories(state),
    isOnline: selectors.getIsOnline(state),
  }
}

const mapDispatchToProps = {
  loadInitialStories: actions.loadInitialStories,
  loadStories: actions.loadStories,
}

export default connect(mapStateToProps, mapDispatchToProps) (NewsList);
