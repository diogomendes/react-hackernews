import React from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import NewsListItem from '../newsListItem/newsListItem';
import './newsList.scss';

class NewsList extends React.Component {
  constructor() {
    super();

    this.onLoadMore = this.onLoadMore.bind(this);
  }

  componentDidMount() {
    this.props.loadInitialStories();
  }

  onLoadMore() {
    if (!this.props.isLoading) {
      this.props.loadStories();
    }
  }

  renderLoader() {
    return <h3 key="loader">Loading...</h3>;
  }

  renderListItems() {
    return this.props.stories.map(story =>
        story && <NewsListItem key={story.id} {...story}/>
    );
  }

  render() {
    const { shouldLoadMoreStories, isOnline } = this.props;

    return (
      <div className="newslist-container">
        <header className="newslist-header">
          <h1 className="newslist-title">Hacker news</h1>
        </header>
        <main className="newslist-body">
          <InfiniteScroll
            loadMore={this.onLoadMore}
            hasMore={isOnline && shouldLoadMoreStories}
            initialLoad={false}
            loader={this.renderLoader()}
          >
            <ul className="newslist">
              {this.renderListItems()}
            </ul>
          </InfiniteScroll>
          {!isOnline &&
            <div className="newslist-offline">
              Offline. Please check network connection.
            </div>
          }
        </main>
      </div>
    );
  }
}

NewsList.propTypes = {
  stories: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  shouldLoadMoreStories: PropTypes.bool.isRequired,
  isOnline: PropTypes.bool.isRequired,
  loadInitialStories: PropTypes.func.isRequired,
  loadStories: PropTypes.func.isRequired,
};

export default NewsList;
