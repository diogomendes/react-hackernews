import { createSelector } from 'reselect';

export const getIsOnline = (state) => state.isOnline;
export const getStoryIds = (state) => state.storyIds;
export const getStories = (state) => state.stories;
export const getIsLoading = (state) => state.isLoading;
export const getShouldLoadMoreStories = (state) => state.shouldLoadMoreStories;

export const getNumLoadedStories = createSelector(
  getStories,
  (stories) => {
    return Object.keys(stories).length;
  }
);

// use sorted storyIds list to sort stories
export const getSortedStories = createSelector(
  getStoryIds,
  getStories,
  getNumLoadedStories,
  (storyIds, stories, numLoadedStories) => {
    const sortedStories = [];

    for (let i = 0; i < numLoadedStories; i++) {
      sortedStories.push(stories[storyIds[i]]);
    }

    return sortedStories;
  }
);
