import { call, put, all, select, takeEvery } from 'redux-saga/effects';
import * as cache from '../../cache';
import * as api from '../../api';
import * as newsListActions from './actions';
import { getStoryIds, getNumLoadedStories } from './selectors';

const NUM_STORIES = 20;

function* loadInitialStories() {
  yield put(newsListActions.setLoadingStatus(true));

  const storyIds = yield fetchStoryIds();

  if (storyIds) {
    const initialStoryIds = storyIds.slice(0, NUM_STORIES);

    // fetch stories in parallel
    yield all(initialStoryIds.map(id => call(fetchStory, id)));
  } else {
    // offline
    yield put(newsListActions.setIsOnline(false));
  }

  yield put(newsListActions.setLoadingStatus(false));
}

function* loadStories() {
  yield put(newsListActions.setLoadingStatus(true));

  const storyIds = yield select(getStoryIds);
  const numLoadedStories = yield select(getNumLoadedStories);

  if (storyIds) {
    const nextStoryIds = storyIds.slice(numLoadedStories, numLoadedStories + NUM_STORIES);

    // no more stories to fetch
    if (nextStoryIds.length === 0) {
      yield put(newsListActions.setShouldLoadMoreStories(false));
      return;
    }

    // fetch stories in parallel
    const nextStories = yield all(nextStoryIds.map(id => call(fetchStory, id)));

    // offline
    if (nextStories.some(story => typeof story === 'undefined')) {
      yield put(newsListActions.setIsOnline(false));
    }
  }

  yield put(newsListActions.setLoadingStatus(false));
}

function* fetchStoryIds() {
  let storyIds;

  try {
    storyIds = yield api.fetchStoryIds();
  } catch (e) {
    storyIds = cache.getCachedStoryIds();
  }

  if (storyIds) {
    yield put(newsListActions.loadStoryIdsSuccess(storyIds));
  }

  return storyIds;
}

function* fetchStory(storyId) {
  const cachedStory = cache.getCachedStory(storyId);
  let story;

  try {
    // prefer using cached value to avoid unnecessary api calls
    story = cachedStory ?
      cachedStory :
      yield api.fetchStory(storyId);
  } catch(e) {
    // fetchStory() call failures are handled in loadStories()
  } finally {
    if (story) {
      yield put(newsListActions.loadStorySuccess(story));
    }

    return story;
  }
}

const sagasList = [
  takeEvery(newsListActions.LOAD_INITIAL_STORIES, loadInitialStories),
  takeEvery(newsListActions.LOAD_STORIES, loadStories)
];

function* newsListSagas() {
  yield all(sagasList);
}

export default newsListSagas;
