export const LOAD_INITIAL_STORIES = 'LOAD_INITIAL_STORIES';
export const LOAD_STORIES = 'LOAD_STORIES';
export const LOAD_STORY_IDS_SUCCESS = 'LOAD_STORY_IDS_SUCCESS';
export const LOAD_STORY_SUCCESS = 'LOAD_STORY_SUCCESS';
export const SET_LOADING_STATUS = 'SET_LOADING_STATUS';
export const SHOULD_LOAD_MORE = 'SHOULD_LOAD_MORE';
export const SET_IS_ONLINE = 'SET_IS_ONLINE';

export function loadInitialStories() {
  return {
    type: LOAD_INITIAL_STORIES,
  }
}

export function loadStories() {
  return {
    type: LOAD_STORIES,
  }
}

export function loadStoryIdsSuccess(data) {
  return {
    type: LOAD_STORY_IDS_SUCCESS,
    data
  }
}

export function loadStorySuccess(data) {
  return {
    type: LOAD_STORY_SUCCESS,
    data
  }
}

export function setLoadingStatus(status) {
  return {
    type: SET_LOADING_STATUS,
    status
  }
}

export function setShouldLoadMoreStories(shouldLoadMoreStories) {
  return {
    type: SHOULD_LOAD_MORE,
    shouldLoadMoreStories
  }
}

export function setIsOnline(isOnline) {
  return {
    type: SET_IS_ONLINE,
    isOnline
  }
}
