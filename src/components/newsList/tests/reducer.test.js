import * as newsListActions from '../actions';
import newsList from '../reducer';
import { mockStory, mockQuestion } from '../../../../__mocks__/mocks';

describe('newsList/reducer', () => {
  it('should return the initial state', () => {
    expect(newsList(undefined, {})).toEqual({
      isOnline: true,
      storyIds: [],
      stories: {},
      isLoading: false,
      shouldLoadMoreStories: true,
    });
  });

  it('should add loaded storyIds', () => {
    const action = {
      type: newsListActions.LOAD_STORY_IDS_SUCCESS,
      data: ['123', '456', '789']
    };

    expect(newsList(undefined, action)).toEqual({
      isOnline: true,
      storyIds: ['123', '456', '789'],
      stories: {},
      isLoading: false,
      shouldLoadMoreStories: true,
    });
  });

  it('should add loaded stories', () => {
    const state = {
      stories: {
        [mockStory.id]: mockStory
      }
    };

    const action = {
      type: newsListActions.LOAD_STORY_SUCCESS,
      data: mockQuestion,
    };

    expect(newsList(state, action)).toEqual({
      stories: {
        [mockStory.id]: mockStory,
        [mockQuestion.id]: mockQuestion
      }
    });
  });

  it('should update loading status', () => {
    const action = {
      type: newsListActions.SET_LOADING_STATUS,
      status: true
    };

    expect(newsList(undefined, action)).toEqual({
      isOnline: true,
      storyIds: [],
      stories: {},
      isLoading: true,
      shouldLoadMoreStories: true,
    });
  });

  it('should update flag indicating whether more data should be loaded', () => {
    const action = {
      type: newsListActions.SHOULD_LOAD_MORE,
      shouldLoadMoreStories: false
    };

    expect(newsList(undefined, action)).toEqual({
      isOnline: true,
      storyIds: [],
      stories: {},
      isLoading: false,
      shouldLoadMoreStories: false,
    });
  });

  it('should update flag indicating whether there is a network connection', () => {
    const action = {
      type: newsListActions.SET_IS_ONLINE,
      isOnline: false
    };

    expect(newsList(undefined, action)).toEqual({
      isOnline: false,
      storyIds: [],
      stories: {},
      isLoading: false,
      shouldLoadMoreStories: true,
    });
  });
});