import React from 'react';
import { shallow, mount } from 'enzyme';
import NewsList from '../newsList';
import { mockStory, mockQuestion } from '../../../../__mocks__/mocks';
describe('newsList', () => {
  const mockNewsList = [mockStory, mockQuestion];

  it('renders the news list and makes initial call to fetch stories', () => {
    const loadInitialStoriesSpy = jest.fn();

    const newsList = shallow(
      <NewsList
        isOnline
        stories={mockNewsList}
        isLoading={false}
        loadInitialStories={loadInitialStoriesSpy}
        loadStories={() => {}}
        shouldLoadMoreStories
      />
    );

    expect(newsList).toMatchSnapshot();
    expect(loadInitialStoriesSpy).toHaveBeenCalled();
  });
});

