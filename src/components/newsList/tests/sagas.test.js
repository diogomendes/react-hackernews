import SagaTester from 'redux-saga-tester';
import newsListSagas from '../sagas';
import * as actions from '../actions';
import newsList from '../reducer';
import * as cache from '../../../cache';
import * as api from '../../../api';

jest.mock('../../../api');
jest.mock('../../../cache');

describe('newsList/sagas', () => {
  const storyIds = [1, 2, 3];
  const stories = {
    1: {id: 1, time: 1},
    2: {id: 2, time: 2},
    3: {id: 3, time: 3}
  }
  let sagaTester = null;

  describe('loadInitialStories', () => {
    beforeEach(() => {
      sagaTester = new SagaTester({ reducers : { newsList } });
      sagaTester.start(newsListSagas);
    });
  
    it('should fetch storyIds and the initial stories from the api', () => {
      // mock api and cache calls
      api.fetchStoryIds.mockImplementation(() => storyIds);
      api.fetchStory.mockImplementation((id) => stories[id]);
      cache.getCachedStory.mockImplementation(() => null);

      // dipatch action and test side effects
      sagaTester.dispatch(actions.loadInitialStories());

      const calledActions = sagaTester.getCalledActions();

      // expect all actions to be called correctly and in the correct order
      expect(calledActions[0]).toEqual(actions.loadInitialStories());
      expect(calledActions[1]).toEqual(actions.setLoadingStatus(true));
      expect(calledActions[2]).toEqual(actions.loadStoryIdsSuccess(storyIds));
      expect(calledActions[3]).toEqual(actions.loadStorySuccess(stories[1]));
      expect(calledActions[4]).toEqual(actions.loadStorySuccess(stories[2]));
      expect(calledActions[5]).toEqual(actions.loadStorySuccess(stories[3]));
      expect(calledActions[6]).toEqual(actions.setLoadingStatus(false));
    });
  
    it('should use storyIds and the initial stories from cache', () => {
      // mock api and cache calls
      api.fetchStoryIds.mockImplementation(() => { throw new Error });
      cache.getCachedStoryIds.mockImplementation(() => storyIds);
      cache.getCachedStory.mockImplementation((id) => stories[id]);

      // dipatch action and test side effects
      sagaTester.dispatch(actions.loadInitialStories());

      const calledActions = sagaTester.getCalledActions();

      // expect all actions to be called correctly and in the correct order
      expect(calledActions[0]).toEqual(actions.loadInitialStories());
      expect(calledActions[1]).toEqual(actions.setLoadingStatus(true));
      expect(calledActions[2]).toEqual(actions.loadStoryIdsSuccess(storyIds));
      expect(calledActions[3]).toEqual(actions.loadStorySuccess(stories[1]));
      expect(calledActions[4]).toEqual(actions.loadStorySuccess(stories[2]));
      expect(calledActions[5]).toEqual(actions.loadStorySuccess(stories[3]));
      expect(calledActions[6]).toEqual(actions.setLoadingStatus(false));
    });
  
    it('should set the status to offline without successful api responses or cached data', () => {
      // mock api and cache calls
      api.fetchStoryIds.mockImplementation(() => { throw new Error });
      cache.getCachedStoryIds.mockImplementation(() => null);

      // dipatch action and test side effects
      sagaTester.dispatch(actions.loadInitialStories());

      const calledActions = sagaTester.getCalledActions();

      // expect all actions to be called correctly and in the correct order
      expect(calledActions[0]).toEqual(actions.loadInitialStories());
      expect(calledActions[1]).toEqual(actions.setLoadingStatus(true));
      expect(calledActions[2]).toEqual(actions.setIsOnline(false));
      expect(calledActions[3]).toEqual(actions.setLoadingStatus(false));
    });
  });

  /* **** XXXXXXXXXXXXXXXXXXXXXXXXXXXX***** */

  describe('loadStories', () => {
    const initialState = {
      isOnline: true,
      storyIds: [1, 2, 3, 4, 5, 6],
      stories,
      isLoading: false,
      shouldLoadMoreStories: true,
    };
    const newStories = {
      4: {id: 4, time: 4},
      5: {id: 5, time: 5},
      6: {id: 6, time: 6}
    }

    beforeEach(() => {
      sagaTester = new SagaTester({
        initialState,
        reducers : newsList
      });
      sagaTester.start(newsListSagas);
    });
  
    it('should fetch next stories from the api', () => {
      // mock api and cache calls
      api.fetchStory.mockImplementation((id) => newStories[id]);
      cache.getCachedStory.mockImplementation(() => null);

      // dipatch action and test side effects
      sagaTester.dispatch(actions.loadStories());

      const calledActions = sagaTester.getCalledActions();

      // expect all actions to be called correctly and in the correct order
      expect(calledActions[0]).toEqual(actions.loadStories());
      expect(calledActions[1]).toEqual(actions.setLoadingStatus(true));
      expect(calledActions[2]).toEqual(actions.loadStorySuccess(newStories[4]));
      expect(calledActions[3]).toEqual(actions.loadStorySuccess(newStories[5]));
      expect(calledActions[4]).toEqual(actions.loadStorySuccess(newStories[6]));
      expect(calledActions[5]).toEqual(actions.setLoadingStatus(false));
    });

    it('should fetch next stories from cache', () => {
      // mock api and cache calls
      api.fetchStory.mockImplementation((id) => { throw new Error });
      cache.getCachedStory.mockImplementation((id) => newStories[id]);

      // dipatch action and test side effects
      sagaTester.dispatch(actions.loadStories());

      const calledActions = sagaTester.getCalledActions();

      // expect all actions to be called correctly and in the correct order
      expect(calledActions[0]).toEqual(actions.loadStories());
      expect(calledActions[1]).toEqual(actions.setLoadingStatus(true));
      expect(calledActions[2]).toEqual(actions.loadStorySuccess(newStories[4]));
      expect(calledActions[3]).toEqual(actions.loadStorySuccess(newStories[5]));
      expect(calledActions[4]).toEqual(actions.loadStorySuccess(newStories[6]));
      expect(calledActions[5]).toEqual(actions.setLoadingStatus(false));
    });

    it('should set the status to offline without successful api responses or cached data', () => {
      // mock api and cache calls
      api.fetchStory.mockImplementation((id) => { throw new Error });
      cache.getCachedStory.mockImplementation((id) => null);

      // dipatch action and test side effects
      sagaTester.dispatch(actions.loadStories());

      const calledActions = sagaTester.getCalledActions();

      // expect all actions to be called correctly and in the correct order
      expect(calledActions[0]).toEqual(actions.loadStories());
      expect(calledActions[1]).toEqual(actions.setLoadingStatus(true));
      expect(calledActions[2]).toEqual(actions.setIsOnline(false));
      expect(calledActions[3]).toEqual(actions.setLoadingStatus(false));
    });
  });
});