import * as newsListActions from '../actions';
import * as selectors from '../selectors';
import { mockStory, mockQuestion } from '../../../../__mocks__/mocks';

describe('newsList/selectors', () => {
  it('should return stories ordered by creation date', () => {
    const storyIds = [1, 2, 3, 4, 5];
    const stories = {
      2: {id: 2, time: 2},
      3: {id: 3, time: 3},
      5: {id: 5, time: 5},
      4: {id: 4, time: 4},
      1: {id: 1, time: 1}
    }
    const state = {
      storyIds,
      stories
    }

    expect(selectors.getSortedStories(state)).toEqual([
      {id: 1, time: 1},
      {id: 2, time: 2},
      {id: 3, time: 3},
      {id: 4, time: 4},
      {id: 5, time: 5},
    ]);
  });
});