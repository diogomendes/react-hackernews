const BASE_URL = 'https://hacker-news.firebaseio.com/v0';

export function fetchStoryIds() {
  return fetch(`${BASE_URL}/newstories.json`).then(response =>
    response.json().then(data => data)
  );
}

export function fetchStory(id) {
  return fetch(`${BASE_URL}/item/${id}.json`).then(response =>
    response.json().then(data => data)
  );
}
