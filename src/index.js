import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import './index.scss';
import NewsListContainer from './components/newsList/container';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Provider store={store}>
    <NewsListContainer/>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
