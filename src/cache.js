import * as newsListActions from './components/newsList/actions';

export const cache = store => next => action => {
  // Call the next dispatch method in the middleware chain.
  const returnValue = next(action);

  switch(action.type) {
    case newsListActions.LOAD_STORY_IDS_SUCCESS:
      cacheStoryIds(action.data);
      break;

    case newsListActions.LOAD_STORY_SUCCESS:
      cacheStory(action.data);
      break;

    default:
      break;
  };

  return returnValue;
};

const cacheStoryIds = (storyIds) => {
  setCachedItem('storyIds', storyIds);
};

const cacheStory = (story) => {
  let stories = getCachedItem('stories');

  if (!stories) {
    stories = {};
  }

  // only cache stories not already in cache 
  if (!stories[story.id]) {
    stories[story.id] = story;
    setCachedItem('stories', stories);
  }
}

export const getCachedStoryIds = () => getCachedItem('storyIds');

export const getCachedStory = (id) => {
  const cachedStories = getCachedItem('stories');
  return cachedStories && cachedStories[id];
};

const getCachedItem = (key) => {
  const cachedItem = localStorage.getItem(key);

  return cachedItem ? JSON.parse(cachedItem) : null;
}

const setCachedItem = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
};