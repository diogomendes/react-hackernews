import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { cache } from './cache';
import newsList from './components/newsList/reducer';
import newsListSagas from './components/newsList/sagas';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(newsList, applyMiddleware(sagaMiddleware, cache));

sagaMiddleware.run(newsListSagas);

export default store;
