export const mockStory = {
  by: 'test-user',
  descendants: 0,
  id: 12345,
  score: 3,
  time: 1535704647,
  title: 'Test Title',
  type: 'story',
  url: 'https://.testurl.com'
};

export const mockQuestion = {
  by: 'test-user',
  descendants: 0,
  id: 12346,
  score: 3,
  text: 'This is some test text',
  time: 1535704655,
  title: 'Test Title',
  type: 'story'
};