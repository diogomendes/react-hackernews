# React Hacker News

A React/Redux app that makes use of Hacker News' api to display news

## Install dependencies

- `npm install`

## Run the app in development mode

- `npm start`
URL: http://localhost:3000

## Build and run the app in development mode

- `npm run build`
- `serve -s build`
URL: http://localhost:5000

## Test the app

- `npm test`
